var color = "bg-primary text-white" 

$(window).on('load', function(){
    $('#loading').delay(2000).fadeOut(1000);
})

$(document).ready( 
    function() {
        $(".accordion").accordion({
            collapsible: true,
            active: false,
            heightStyle: "content",
            beforeActivate: function(e, ui) {
                ui.newHeader.toggleClass(color);
                ui.oldHeader.toggleClass(color);
                ui.newHeader.find("i").toggleClass("fa-angle-down");
                ui.oldHeader.find("i").toggleClass("fa-angle-down");
                ui.newHeader.find("i").toggleClass("fa-angle-up");
                ui.oldHeader.find("i").toggleClass("fa-angle-up");
            }
        });
        $("i").each(function(){
            $(this).change(function(){
                $(this).toggleClass("fa fa-angle-down", "fa fa-angle-up")
            });
        });
        $("#change").click(function(){
            if (color === "bg-primary text-white") {
                $(".bg-primary").toggleClass("bg-danger text-white");
                $(".bg-primary").toggleClass(color);
                color = "bg-danger text-white";
            }
            else {
                $(".bg-danger").toggleClass("bg-primary text-white");
                $(".bg-danger").toggleClass(color);
                color = "bg-primary text-white";
            }
            $("body").toggleClass("red");
            $("body").toggleClass("blue");
            $("button").toggleClass("btn-primary");
            $("button").toggleClass("btn-danger");
        });
    }
);