from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.db import models
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, profile
from .models import Status
import time

# Create your tests here.

# Ini functional test buat lokal
class FunctionalTestLokal(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()

        # gitlab CI settings
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_header_content(self):
        driver = self.driver
        driver.get(self.live_server_url)
        

        # Check Header conatain text Hello, Apa Kabar
        header = driver.find_element_by_tag_name('h1')
        self.assertEqual("Hello, Apa kabar?", header.text)

    def test_form_exist_in_html(self):
        driver = self.driver
        driver.get(self.live_server_url)
        

        # Check that there is exist a form element in html
        self.assertTrue(driver.find_elements_by_tag_name('form'))

    def test_html_has_dark_background(self):
        driver = self.driver
        driver.get(self.live_server_url)
        

        # Check the most outer div use Bootstrap class bg-dark
        self.assertTrue('bg-dark' in driver.find_element_by_id('outer').get_attribute('class'))
    
    def test_html_has_white_font_color(self):
        driver = self.driver
        driver.get(self.live_server_url)
        

        # Check the most inner div use Bootstrap class text-white
        self.assertTrue('text-white' in driver.find_element_by_id('inner').get_attribute('class'))

    def test_submit_sentence(self):
        driver = self.driver
        driver.get(self.live_server_url)
        

        str_input = "Coba Coba"

        # find input field by name message in html and fill it with the string
        form = driver.find_element_by_name('message')
        form.send_keys(str_input)

        #find the submit button and hit enter
        submit = driver.find_element_by_tag_name('button')
        submit.send_keys(Keys.RETURN)
        

        # assert the string is in web page
        self.assertIn(str_input, driver.page_source)

    def test_loading_page(self):
        driver = self.driver
        driver.get(self.live_server_url+"/profile")
        self.assertTrue(driver.find_element_by_id('loading').value_of_css_property("display") == "block")
        time.sleep(4)
        self.assertTrue(driver.find_element_by_id('loading').value_of_css_property("display") == "none")
    
    def test_change_theme(self): 
        driver = self.driver
        driver.get(self.live_server_url+"/profile")
        self.assertEqual(driver.find_element_by_tag_name('body').value_of_css_property("background-image"), 'url("https://images.unsplash.com/photo-1464618663641-bbdd760ae84a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=62a77bae48158b98b0acdf9fa674410e&auto=format&fit=crop&w=1500&q=80")')
        submit = driver.find_element_by_id('change')
        submit.send_keys(Keys.RETURN)
        self.assertEqual(driver.find_element_by_tag_name('body').value_of_css_property("background-image"), 'url("https://images.unsplash.com/photo-1505163283257-f1cc16f10129?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b87aa1b75746bd1247d519a36f095c62&auto=format&fit=crop&w=1489&q=80")')

    def test_accordion(self):
        driver = self.driver
        driver.get(self.live_server_url+"/profile")
        panel = driver.find_element_by_class_name("accordion-body")
        header = driver.find_element_by_class_name("header")
        self.assertEqual(panel.value_of_css_property("display"), "none")
        header.send_keys(Keys.ENTER)
        self.assertEqual(panel.value_of_css_property("display"), "block")



class LandingTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_html_contain_hello(self):
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        Status.objects.create(date=timezone.now(), message='Ini Tes')
        number_of_objects = Status.objects.all().count()
        self.assertEqual(number_of_objects, 1)

    def test_message_max_length(self):
        Status.objects.create(date=timezone.now(), message='Ini Tes')
        status = Status.objects.get(id=1)
        max_length = status._meta.get_field('message').max_length
        self.assertEqual(max_length, 300)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/add-status', data={
            'date': '2017-10-12 14:14',
            'message': 'Ini buat tes lagi dong'
        })
        number_of_objects = Status.objects.all().count()
        self.assertEqual(number_of_objects, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Ini buat tes lagi dong', html_response)

class ProfileTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)
    
    def test_using_correct_template(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_contain_name(self):
        new_response = self.client.get('/profile')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Name', html_response)
    
    def test_contain_npm(self):
        new_response = self.client.get('/profile')
        html_response = new_response.content.decode('utf8')
        self.assertIn('NPM', html_response)