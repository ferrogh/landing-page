# Generated by Django 2.1.1 on 2018-10-10 12:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
