from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout
import requests


# Create your views here.
def books(request):
    img_url = request.session.get('image_url')
    name = request.session.get('display_name')
    return render(request, 'books.html', {'name': name, 'img_url': img_url})
    

def getApi(request):
    key = "quilting"
    if 'search' in request.GET:
        key = request.GET.get('search')
    responses = requests.get('https://www.googleapis.com/books/v1/volumes?q='+key).json()
    new_response = []
    try:
        for response in responses['items']:
            new_response.append({
                'title': response['volumeInfo']['title'] if 'title' in response['volumeInfo'] else '',
                'authors': response['volumeInfo']['authors'] if 'authors' in response['volumeInfo'] else ['Unknown'],
                'image': response['volumeInfo']['imageLinks']['smallThumbnail'] if 'imageLinks' in response['volumeInfo'] else '',
                'description': response['volumeInfo']['description'] if 'description' in response['volumeInfo'] else 'No Description'
            })
    except:
        new_response = []
    return JsonResponse({'data': new_response})


def log_out(request):
    logout(request)
    return redirect('/books')

def get_favorite(request):
    num_favorites = request.session.get('num_favorites', 0)
    return JsonResponse({'num_favorites': num_favorites})

def set_favorite(request):
    change = 0
    if 'change' in request.GET:
        change = int(request.GET.get('change'))
    num_favorites = request.session.get('num_favorites', 0)
    num_favorites =  num_favorites + change
    request.session['num_favorites'] = num_favorites
    return JsonResponse({'num_favorites': num_favorites})


def set_extra_data_on_session(backend, strategy, details, response, user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('image_url', response['image'].get('url'))
        strategy.session_set('display_name', response['displayName'])
