function getfavorite() {
    $.ajax({
        url: "/books/getfavorite",
        success: function(result) {
            $("#counter").html(result.num_favorites);
        }
    });
}

function setfavorite(change) {
    $.ajax({
        url: "/books/setfavorite/?change=" + change,
        success: function(result) {
            $("#counter").html(result.num_favorites);
        }
    });
}

function search(e){
    if (e.type == "keypress" && e.which != 13) {
        console.log("masuk");
        return false;
    }
    var key = $("input[name='search']").val();
    // default quilting
    if (key == "") key = "quilting";
    $.ajax({
        url: ("/books/getApi/?search=" + key),
        success: function(result) {
            var html = '';
            var books = result.data;
            for (var i = 0; i < books.length; i++) {
                var author = '';
                var authors = books[i].authors;
                for (var j = 0; j < authors.length; j++) {
                    if (j > 0) author += ', ';
                    author += authors[j];
                }
                html += '<tr>';
                html += '<td>' + (i+1) + '</td>';
                html += '<td>' + '<img src="' + books[i].image + '" alt="Its Me!">' + '</td>';
                html += '<td>' + 
                        '<h5>' + books[i].title + '</h5>' +
                        '<h6>' + author + '</h6>' +
                        '<p>' + books[i].description + '</p>' +
                        '</td>';
                html += '<td><i class="far fa-star" style="color:yellow;;"></i></td>'
                html += '</tr>';
                // console.log(authors);
            }
            $('tbody').html(html);
            $("i").click(function(){
                $(this).toggleClass("fas");
                $(this).toggleClass("far");
                if ($(this).hasClass("fas")) setfavorite(1);
                else setfavorite(-1);
                getfavorite()
            });
        }
    });
}


$(document).ready(
    function() {
        getfavorite()
        $("#search").on("click", function(e){
            search(e);
        });
        $("input[name='search']").on("keypress", function(e){
            search(e);
        });
    }
);