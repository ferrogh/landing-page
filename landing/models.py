from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    date = models.DateTimeField(default=timezone.now)
    message = models.CharField(max_length=300)
