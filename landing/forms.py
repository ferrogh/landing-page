from django import forms
from . import models

class StatusForm(forms.ModelForm):
    class Meta:
        model = models.Status
        fields = ('message',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })