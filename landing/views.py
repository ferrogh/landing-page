from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
def index(request):
    response = {
        'form': StatusForm,
        'statuses': Status.objects.all()
    }
    return render(request, 'index.html', response)

def add_status(request):
    form = StatusForm(request.POST or None)
    if request.method == 'POST' and form.is_valid:
        form.save()
    return HttpResponseRedirect('/')

def profile(request):
    return render(request, 'profile.html')

