from django.urls import path
from . import views

urlpatterns = [
    path('', views.books, name='books'),
    path('getApi/', views.getApi, name='getApi'),
    path('getfavorite/', views.get_favorite),
    path('setfavorite/', views.set_favorite)
]