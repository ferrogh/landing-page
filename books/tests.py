from django.test import TestCase, Client
from django.urls import resolve

from .views import books, getApi

# Create your tests here.
class BooksTest(TestCase):
    def test_books_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_using_correct_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')
    
    def test_using_books_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)
    
    def test_get_api_url_exist(self):
        response = Client().get('/books/getApi/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_get_api_func(self):
        found = resolve('/books/getApi/')
        self.assertEqual(found.func, getApi)

    def test_get_favorite_url_exist(self):
        response = Client().get('/books/getfavorite/')
        self.assertEqual(response.status_code, 200)
    
    def test_set_favorite_url_exist(self):
        response = Client().get('/books/setfavorite/')
        self.assertEqual(response.status_code, 200)
