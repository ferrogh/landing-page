from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add-status', views.add_status, name='add_status'),
    path('profile', views.profile, name='profile'),
]